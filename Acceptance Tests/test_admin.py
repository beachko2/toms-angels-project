import unittest
from Roles import Admin


class TestAdmin(unittest.TestCase):
    def setUp(self):
        self.prof1 = Admin(
            'boyland',
            'boyland@uwm.edu',
            '987654321',
            'password1')

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',
    #     schedule     => 'T 10:00am - 3:00pm',
    #                     'W 10:00am - 5:00pm',
    #                     'R 10:00am - 3:00pm',

    # <MODEL 3> TA Details:
    #     name         => 'apoorv',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',
    #     schedule     => 'W 11:00am - 2:00pm',
    #                     'R 3:00pm - 5:00pm',
    #     ta_type      => 'ta'

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'
    #     day          => 'W'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     day          => 'TR'
    #     lab          => 'true'

    def test_valid_login_admin(self):
        #User story: Admin can log in using format: Login <username> <password>
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

    def test_invalid_login_admin(self):
        # Admin is prompted to reenter
        # password when they enter the wrong password
        self.assertEquals(self.ui.command("Login boyland passqword1"), "Invalid Password, try again")
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

    def test_help_admin(self):
        self.ui.command("Login boyland password1")
        # all of these commands should be available to an admin
        self.assertEquals(self.ui.command("help"), "Commands:"
                                                   "Login"
                                                   "Edit"
                                                   "Delete"
                                                   "Create"
                                                   "Assign"
                                                   "View"
                                                   "Send")

    # edit their own contact information (not course assignments)
    def test_edit_information_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit boyland",
                                          "Choose Information to edit: Username, Password, Schedule, Phone: "))

    def test_invalid_course_admin(self):
        # User Story:
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEqual(self.ui.command("Create <CS9667>"), "INVALID COURSE")

    def test_create_course_admin(self):
        # User Story: Admin can create course using format: Create <Class code>
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Create <CS361>"),
                          "CS361 created. Please enter section number, days, and times for lecture: ")
        self.assertEquals(self.ui.command("401 TR 12:00pm - 1:00pm"),
                          "Does this course have a lab? (Yes/No)")
        if self.ui.command("Yes"):
            self.assertEquals(self.ui.command("Yes"),
                              "Please enter lab section number and times: ")
            self.assertEquals(self.ui.command("801 W 11:30am - 12:45pm"),
                              "Lab created successfully")
        else:
            self.assertEquals(self.ui.command("No"),
                              "Course without lab created successfully")

    def test_create_course_duplicate_admin(self):
        # User Story: Must check for duplicate before creating course.
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Create <CS361>"),
                          "CS361 already created. To edit, please enter section number, days, and times for lecture: ")
        self.assertEquals(self.ui.command("401 TR 12:00pm - 1:00pm"),
                          "Does this course have a lab? (Yes/No)")
        if self.ui.command("Yes"):
            self.assertEquals(self.ui.command("Yes"),
                              "Please enter lab section number and times: ")
            self.assertEquals(self.ui.command("801 W 11:30am - 12:45pm"),
                              "Lab created successfully")
        else:
            self.assertEquals(self.ui.command("No"),
                              "Course without lab created successfully")

    def test_create_account_TA_admin(self):
        # User story: Admin can create account using command format: Create <username>
        # User story: Only username is required as information
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create apoorv"),
                         "Account for apoorv added successfully")

    def test_create_account_prof_admin(self):
        # User story: Admin can create account using command format: Create <username>
        # User story: Only username is required as information
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create rock"),
                         "Account for rock added successfully")

    def test_delete_account_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Delete apoorv"),
                         "Account for apoorv deleted successfully")

    def test_delete_account_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Delete rock"),
                         "Account for rock deleted successfully")

    def test_edit_account_TA_admin(self):
        # User story: Admin is able to edit any existing account
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")

    def test_edit_username_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Username"),
                          "Choose new Username: ")
        self.assertEquals(self.ui.command("apoorv2"),
                          "Username successfully changed to apoorv2")

    def test_edit_invalid_username_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Username"),
                          "Choose new Username: ")
        self.assertEquals(self.ui.command("apoorv"),
                          "Invalid, please choose different username")

    def test_edit_password_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Password"),
                          "Choose new Password: ")
        self.assertEquals(self.ui.command("password2"),
                          "Password successfully changed")

    def test_edit_invalid_password_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Password"),
                          "Choose new Password: ")
        self.assertEquals(self.ui.command("password1"),
                          "Invalid, please choose different password.")

    def test_edit_phone_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Phone"),
                          "Enter new phone number: ")
        self.assertEquals(self.ui.command("4145556666"),
                          "Phone number successfully changed")

    def test_edit_invalid_phone_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Phone"),
                          "Enter new phone number: ")
        self.assertEquals(self.ui.command("aaa"),
                          "Invalid input.")

    def test_edit_schedule_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Schedule"),
                          "Enter new schedule: ")
        self.assertEquals(self.ui.command("W 9:00am - 5:00pm, F 3:00pm - 5:00pm"),
                          "Schedule successfully changed.")

    def test_edit_invalid_schedule_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Schedule"),
                          "Enter new schedule: ")
        self.assertEquals(self.ui.command("WF"),
                          "Invalid input.")

    def test_edit_account_prof_admin(self):
        # User story: Admin is able to edit any existing account
        self.assertEquals(self.ui.command("Login boyland@uwm.edu password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")

    def test_edit_username_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Username"),
                          "Choose new Username: ")
        self.assertEquals(self.ui.command("rock2"),
                          "Username successfully changed to rock2")

    def test_edit_invalid_username_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Username"),
                          "Choose new Username: ")
        self.assertEquals(self.ui.command("rock"),
                          "Invalid, please choose different username")

    def test_edit_password_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Password"),
                          "Choose new Password: ")
        self.assertEquals(self.ui.command("password2"),
                          "Password successfully changed")

    def test_edit_invalid_password_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Password"),
                          "Choose new Password: ")
        self.assertEquals(self.ui.command("password1"),
                          "Invalid, please choose different password.")

    def test_edit_phone_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Phone"),
                          "Enter new phone number: ")
        self.assertEquals(self.ui.command("4145556666"),
                          "Phone number successfully changed")

    def test_edit_invalid_phone_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Phone"),
                          "Enter new phone number: ")
        self.assertEquals(self.ui.command("aaa"),
                          "Invalid input.")

    def test_edit_schedule_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Schedule"),
                          "Enter new schedule: ")
        self.assertEquals(self.ui.command("T 9:00am - 5:00pm, W 3:00pm - 5:00pm"),
                          "Schedule successfully changed.")

    def test_edit_invalid_schedule_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit apoorv"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Schedule"),
                          "Enter new schedule: ")
        self.assertEquals(self.ui.command("TW"),
                          "Invalid input.")

    def test_send_out_notification_all_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Send ALL <message>."), "<message> sent successfully to all.")

    def test_assign_professor_to_courses_admin(self):
        # User Story: The admin is able to assign professors to courses
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign rock CS361"), "Jason Rock has been assigned to CS361")

    def test_assign_ta_to_courses_admin(self):
        # User story: The admin is able to assign a TA to courses
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign apoorv CS361"), "Apoorv has been assigned to CS361")

    def test_assigning_ta_to_lab_admin(self):
        # User story: The admin is able to assign a TA to specific lab
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign apoorv CS361"), "Apoorv has been assigned to CS361")
        self.assertEqual(self.ui.command("Assign apoorv to CS361 Section 801"),
                         "Apoorv has been assigned to CS361 Section 801")

    # send out notifications to their TAs via UWM email
    def test_EmailSpecificTA_admin(self):
        self.assertEquals(self.ui.command("Send apoorv <message>"),
                          "Email was successfully sent to apoorv.")
        self.assertEquals(self.ui.command("Send shudbfg <message>"),
                          "Username doesn't exist.")

    # read public contact information of all users
    def test_ReadAllUsersContactInformation_admin(self):
        self.assertEquals(self.ui.command("View <ALL>"),
                          "Name: Apoorv Lastname"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: 9999999919")
