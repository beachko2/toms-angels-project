import unittest
from Roles import TA


class TestTA(unittest.TestCase):
    def setUp(self):
        self.ta1 = TA(
            'apoorv',
            'employee1@company.com',
            '987654321',
            'password1', )

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',
    #     schedule     => 'T 10:00am - 3:00pm',
    #                     'W 10:00am - 5:00pm',
    #                     'R 10:00am - 3:00pm',

    # <MODEL 3> TA Details:
    #     name         => 'apoorv',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',
    #     schedule     => 'W 11:00am - 2:00pm',
    #                     'R 3:00pm - 5:00pm',
    #     ta_type      => 'ta'

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'
    #     day          => 'W'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     day          => 'TR'
    #     lab          => 'true'


    # TA can log in with correct credentials
    def test_login_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

    # TA is prompted to reenter
    # password when they enter the wrong password
    def test_invalid_login_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Invalid Password, try again")
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

    # only these commands should be available to a TA
    def test_help_ta(self):
        self.ui.command("Login prasada@uwm.edu password1")
        self.assertEquals(self.ui.command("help"), "Commands:"
                                                   "login"
                                                   "logout"
                                                   "edit"
                                                        "contact"
                                                        "schedule"
                                                   "view"
                                                        "courseassignments"
                                                        "taassignments"
                                                   "send")

    # TA wants to edit their information
    def test_edit_information_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")

    # TA wants to edit their information but does not enter any of the available commands
    def test_edit_information_ta_invalid(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"), "Choose Information to edit: Password, Email, Phone: ")
        self.assertEquals(self.ui.command("Username"), "Invalid command.")

    # TA wants to edit their password
    def test_edit_password_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")

        self.assertEquals(self.ui.command("Password"), "Enter new password")
        self.assertEquals(self.ui.command("newpassword1"), "Password set")

    # TA wants to edit password but enters a blank password
    def test_edit_password_ta_invalid(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")

        self.assertEquals(self.ui.command("Password"), "Enter new password")
        self.assertEquals(self.ui.command(""), "Invalid input.")

    #TA wants to edit their email
    def test_edit_schedule_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")
        self.assertEquals(self.ui.command("Email"),
                          "Enter new email: ")
        self.assertEquals(self.ui.command("prasada2@uwm.edu"),
                          "Schedule successfully changed.")

    # TA wants to edit their email but they enter invalid email
    def test_edit_schedule_ta_invalid(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")
        self.assertEquals(self.ui.command("Email"), "Enter new email: ")
        self.assertEquals(self.ui.command("prasada@"), "Invalid input.")

    #Ta wants to edit their phone number
    def test_edit_phone_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")

        self.assertEquals(self.ui.command("Phone"), "Enter new phone number")
        self.assertEquals(self.ui.command("4140002330"), "Phone number set")

    # Ta wants to edit their phone number but enters invalid input
    def test_edit_phone_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit prasada"),
                          "Choose Information to edit: Password, Email, Phone: ")

        self.assertEquals(self.ui.command("Phone"), "Enter new phone number")
        self.assertEquals(self.ui.command("0012"), "Invalid input.")

    #TA wants to edit their schedule
    def test_edit_schedule_ta(self):
        self.test_login_ta()
        self.assertEqual(self.ui.command("Edit prasada schedule"), "Enter new schedule: ")
        self.assertEquals(self.ui.command("W 9:00am - 5:00pm, F 3:00pm - 5:00pm"),
                          "Schedule successfully changed.")

    # TA wants to edit their schedule but they enter invalid schedule
    def test_edit_schedule_ta_invalid(self):
        self.test_login_ta()
        self.assertEqual(self.ui.command("Edit prasada schedule"), "Enter new schedule: ")
        self.assertEquals(self.ui.command("WF"), "Invalid input.")


    # TAs cannot view course assignments
    def test_view_course_assignments_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("view courseassignments"), "You do not have course assignment permissions")

    # view TA assignments (for one TA)
    def test_view_one_existing_TA_assignments_prof(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("view taassignments prasada"),
                          "Course assignment: CS361"
                          "Lab assignment: 803"
                          "Status: TA")

    # view TA assignments (for one TA)
    def test_view_one_existing_TA_assignments_prof(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("view taassignments beachko2"), "Can't view course assignments for other TA's.")

    # TAs cannot create courses
    def test_create_course_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Create <CS351>"), "You do not have class creation permissions")

    # send out notifications to their TAs via UWM email
    def test_EmailSpecificTA_prof(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Send rock <message>"),
                            "Email was successfully sent to TA@uwm.edu")
        self.assertEquals(self.ui.command("Send shudbfg <message>"),
                            "Username shudbfg doesn't exist")

    # Ta cannot send to all users
    def test_ReadAllUsersContactInformation_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Send <ALL> message"),
                          "You do not have send all permissions")

    # read public contact information of all users
    def test_ReadAllUsersContactInformation_ta(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("View <ALL>:"),
                          "Name: Apoorv Prasad"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: 9999999919")
