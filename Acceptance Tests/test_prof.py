import unittest
from Roles import Professor


class TestProfessor(unittest.TestCase):
    def setUp(self):
        self.prof1 = Professor(
            'rock',
            'rock@uwm.edu',
            '987654321',
            'password1', )

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 3> TA Details:
    #     name         => 'prasada',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',
    #     ta_type      => 'ta'

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     lab          => 'true'

    # Login Tests
    def test_valid_login_prof(self):
        # Prof can log in with correct credentials
        self.assertEquals(self.ui.command("login rock password1"), "Access granted.")

    def test_invalid_login_prof(self):
        # Prof is prompted to reenter
        # password when they enter the wrong password
        self.assertEquals(self.ui.command("login rock passqword1"), "Invalid password, please try again.")
        self.test_valid_login_prof()

    def test_view_commands_prof(self):
        self.test_valid_login_prof()
        # only these commands should be available to a prof
        self.assertEquals(self.ui.command("help"),
                          "Commands:"
                          "login"
                          "logout"
                          "edit"
                                "contact"
                                "taassignments"
                          "view"
                                "courseassignments"
                                "taassignments"
                          "assign"
                                "ta"
                                "professor"
                          "send"
                          "delete")

    # Prof wants to edit their information
    def test_edit_information_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")

    # Prof wants to edit their information but does not enter any of the available commands
    def test_edit_information_prof_invalid(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Username"), "Invalid command.")

    # Prof wants to edit their password
    def test_edit_password_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")

        self.assertEquals(self.ui.command("Password"), "Enter new password")
        self.assertEquals(self.ui.command("newpassword1"), "Password set")

    # Prof wants to edit password but enters a blank password
    def test_edit_password_prof_invalid(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")

        self.assertEquals(self.ui.command("Password"), "Enter new password")
        self.assertEquals(self.ui.command(""), "Invalid input.")

    # Prof wants to edit their email
    def test_edit_schedule_prof(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Email, Phone: ")
        self.assertEquals(self.ui.command("Email"),
                          "Enter new email: ")
        self.assertEquals(self.ui.command("rock2@uwm.edu"),
                          "Schedule successfully changed.")

    # Prof wants to edit their email but they enter invalid email
    def test_edit_schedule_prof_invalid(self):
        self.test_login_ta()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Email, Phone: ")
        self.assertEquals(self.ui.command("Email"), "Enter new email: ")
        self.assertEquals(self.ui.command("rock@"), "Invalid input.")

    # Prof wants to edit their phone number
    def test_edit_phone_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")

        self.assertEquals(self.ui.command("Phone"), "Enter new phone number")
        self.assertEquals(self.ui.command("4140002330"), "Phone number set")

    # Prof wants to edit their phone number but enters invalid input
    def test_edit_phone_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Password, Schedule, Phone: ")

        self.assertEquals(self.ui.command("Phone"), "Enter new phone number")
        self.assertEquals(self.ui.command("0012"), "Invalid input.")

    def test_view_nonexistent_course_assignments_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewcourseassignments CS9999"), "Error, course does not exist.")

    # view TA assignments (for one TA)
    def test_view_one_existing_TA_assignments_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewtaassignments rock"),
                          "Course assignment: CS361"
                          "Lab assignment: 803"
                          "Status: TA")

    #invalid view TA assignment
    def test_view_one_nonexistent_TA_assignments_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewtaassignments shudbfg"), "Error, username does not exist.")

    # view TA assignments (for all TAs)
    def test_view_all_existing_TA_assignments_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewtaassignments all"),
                          "ePanther: prasada"
                          "Course assignment: CS361"
                          "Lab assignment: 803"
                          "Status: TA")



    # send out notifications to their TAs via UWM email
    def test_message_specific_ta_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("messageta prasada, message"),
                          "Email was successfully sent to prasada.")
        self.assertEquals(self.ui.command("messageta shudbfg, message"),
                          "Username shudbfg doesn't exist.")

    # professors cannot assign other professors to courses
    def test_cannot_assign_professor_to_courses_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assignprofessortocourse rds, CS361"),
                         "You do not have permission to assign professors to courses.")

    # admin can assign professors to courses
    def test_cannot_assign_professor_to_courses_prof(self):
        self.assertEquals(self.ui.command("login boyland password1"), "Access granted.")
        self.assertEqual(self.ui.command("assignprofessortocourse rds, CS361"), "rds has been assigned to CS361.")

    # professors can assign TAs to lab sections
    def test_assign_existing_ta_to_existing_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assigntatolab prasada, CS361, 803"), "prasada has been assigned to CS361-803.")

    def test_assign_existing_ta_to_existing_course_existing_lab_twice_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assigntatolab prasada, CS361, 803"), "prasada has been assigned to CS361-803.")
        self.assertEqual(self.ui.command("assigntatolab prasada, CS361, 803"), "Error, TA is already assigned to CS361-803.")

    def test_assign_existing_ta_to_existing_course_nonexistent_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assigntatolab prasada , CS361, 9999"), "Error, lab does not exist.")

    def test_assign_existing_ta_to_nonexistent_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assigntatolab prasada, CS9999, 803"), "Error, course does not exist.")

    def test_assign_nonexistent_ta_to_existing_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("assigntatolab lkjlhlkjh , CS361, 803"), "Error, TA does not exist.")

    # professors can remove TAs from lab sections
    def test_remove_existing_ta_from_existing_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("removetafromlab prasada, CS361, 803"),
                         "prasada has been removed from CS361-803.")

    def test_remove_existing_ta_from_existing_course_existing_lab_twice_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("removetafromlab prasada, CS361, 803"), "prasada has been removed CS361-803.")
        self.assertEqual(self.ui.command("removetafromlab prasada, CS361, 803"),
                         "Error, TA is already not assigned to CS361-803.")

    def test_remove_existing_ta_from_existing_course_nonexistent_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("removetafromlab prasada , CS361, 9999"), "Error, lab does not exist.")

    def test_remove_existing_ta_from_nonexistent_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("removetafromlab prasada, CS9999, 803"), "Error, course does not exist.")

    def test_remove_nonexistent_ta_from_existing_course_existing_lab_prof(self):
        self.test_valid_login_prof()
        self.assertEqual(self.ui.command("removetafromlab lkjlhlkjh , CS361, 803"), "Error, TA does not exist.")

    # read public contact information of all users
    def test_view_all_user_contact_information_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewusercontactinformation all"),
                          "Name: Apoorv Prasad"
                          "ePanther: prasada"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: (414) 999-9999"
                          ""
                          "Name: Jayson Rock"
                          "ePanther: rock"
                          "Role: Professor"
                          "Email: rock@uwm.edu"
                          "Phone: (414) 999-9999"
                          "")

    # read public contact information of a user
    def test_view_user_contact_information_prof(self):
        self.test_valid_login_prof()
        self.assertEquals(self.ui.command("viewusercontactinformation prasada"),
                          "Name: Apoorv Prasad"
                          "ePanther: prasada"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: (414) 999-9999")
