class CourseTime:
    # startTime   #int
    # endTime     #int
    # day         #string
    # isOnline    #boolean

    #Precondition: Take in a string. Should be either online or whitespace. Delimited triple <start time> <end time> <days>
    #Side Efferts: Passing arguments of the wrong type (e.g. passing an int when a string is expected) should result
    #       in a TypeError (Links to an external site.), but passing arguments with the wrong value (a string with the
    #       wrong format) should result in a ValueError
    def __init__(self):
        pass

    #Precondition: Doesn't accept any arguements.
    #Postcondition: Return the start or end times as datetime.time objects.
    def __str__(self):
        pass

    #Postcondition: Returns the start time as datetime.time objects
    def start(self):
        pass

    #Postcondition: Returns the end time as datetime.time objects
    def end(self):
        pass

    #PostconditionL Returns true is online, false otherwise.
    def isOnline(self):
        pass

    #Precondition: Takes in instance of CourseTime object
    #Precodnition: Returns true if day and time overlap. Otherwise false.
    def isOverlap(self, coursetime):
        pass

